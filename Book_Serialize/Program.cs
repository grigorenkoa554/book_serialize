﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;

namespace Book_Serialize
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Serialize();// - последовательная запись полей и св-тв объекта в определённый Stream (файл) определённой кодировки (Xml,Binary)
            Deserialize();// - последовательное чтение объекта с определённого потока (файла) определённой кодировки (Xml,Binary)


        }

        static void Deserialize()
        {
            var xmlFormater = new XmlSerializer(typeof(SomeUser));
            using (var streamReader = new StreamReader("some.txt"))
            {
                var myTemp = (SomeUser)xmlFormater.Deserialize(streamReader);
                Console.WriteLine($"Resualt = {myTemp.Age}, {myTemp.FirstName}");
            }


            var binarySome = new BinaryFormatter();
            using (var fileStream = new FileStream("some.bin", FileMode.Open))
            {
                var myBin = (SomeUser)binarySome.Deserialize(fileStream);
                Console.WriteLine($"Resualt = {myBin.Age}, {myBin.FirstName}");
            }
        }

        static void Serialize()
        {
            var city = new City { CityLine = "Some city line" };
            var user = new SomeUser
            {
                Age = 23,
                FirstName = "sdf",
                LastName = "sdf",
                someFiled = "22",
                someFiled2 = "22",
                SomeSpecificField = "sdf",
                Addres = new Addres { AdressLine = "Some addres line", City = city },
                City = city
            };
            var xmlFormater2 = new XmlSerializer(typeof(List<SomeUser>));
            using (var streamWriter = new StreamWriter("some2.txt"))
            {
                xmlFormater2.Serialize(streamWriter, new List<SomeUser> { user });
            }
            var xmlFormater = new XmlSerializer(typeof(SomeUser));
            using (var streamWriter = new StreamWriter("some.txt"))
            {
                xmlFormater.Serialize(streamWriter, user);
            }
            //или так
            var binaryFormatter = new BinaryFormatter();
            using (var streamWriter = new FileStream("some.bin", FileMode.Create))
            {
                binaryFormatter.Serialize(streamWriter, user);
            }
        }
    }

    [Serializable]
    public class SomeUser
    {
        [XmlAttribute]
        public int Age { get; set; }
        public string FirstName { get; set; }
        [XmlElement(ElementName = "ListName")]
        public string LastName { get; set; }
        public string SomeSpecificField { get; set; }

        [NonSerialized]
        public string someFiled;

        public string someFiled2;

        public Addres Addres { get; set; }
        public City City { get; set; }

        //[OnSerializing]
        public void Print()
        {
            Console.WriteLine("before serialize");
        }
    }

    [Serializable]
    public class Addres
    {
        public string AdressLine { get; set; }
        public City City { get; set; }
    }
    [Serializable]
    public class City
    {
        public string CityLine { get; set; }

    }
}
